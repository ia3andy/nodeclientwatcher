define (require)->
  Rickshaw = require 'rickshaw'
  _ = require 'underscore'
  moment = require 'moment'
  class CustomHoverDetail extends Rickshaw.Graph.HoverDetail
    update: (e) ->
      e = e or @lastEvent
      return  unless e
      @lastEvent = e
      return  unless e.target.nodeName.match(/^(path|svg|rect)$/)
      graph = @graph
      eventX = e.offsetX or e.layerX
      eventY = e.offsetY or e.layerY
      domainX = graph.x.invert(eventX)
      stackedData = graph.stackedData
      topSeriesData = stackedData.slice(-1).shift()
      domainIndexScale = d3.scale.linear().domain([topSeriesData[0].x, topSeriesData.slice(-1).shift().x]).range([0, topSeriesData.length])
      approximateIndex = Math.floor(domainIndexScale(domainX))
      dataIndex = Math.min(approximateIndex or 0, stackedData[0].length - 1)
      i = approximateIndex

      while i < stackedData[0].length - 1
        break  if not stackedData[0][i] or not stackedData[0][i + 1]
        if stackedData[0][i].x <= domainX and stackedData[0][i + 1].x > domainX
          dataIndex = i
          break
        if stackedData[0][i + 1] <= domainX
          i++
        else
          i--
      domainX = stackedData[0][dataIndex].x
      formattedXValue = @xFormatter(domainX)
      graphX = graph.x(domainX)
      order = 0
      detail = graph.series.active().map((s) ->
        order: order++
        series: s
        name: s.name
        value: s.stack[dataIndex]
      )
      activeItem = undefined
      sortFn = (a, b) ->
        (a.value.y0 + a.value.y) - (b.value.y0 + b.value.y)

      domainMouseY = graph.y.magnitude.invert(graph.element.offsetHeight - eventY)
      detail.sort(sortFn).forEach ((d) ->
        d.formattedYValue = (if (@yFormatter.constructor is Array) then @yFormatter[detail.indexOf(d)](d.value.y) else @yFormatter(d.value.y))
        d.graphX = graphX
        d.graphY = graph.y(d.value.y0 + d.value.y)
        if domainMouseY > d.value.y0 and domainMouseY and not activeItem
          activeItem = d
          d.active = true
      ), this
      @element.innerHTML = ""
      @element.style.left = graph.x(domainX) + "px"
      if @visible
        @render
          detail: detail
          domainX: domainX
          formattedXValue: formattedXValue
          mouseX: eventX
          mouseY: eventY

  class XAxis extends Rickshaw.Graph.Axis.Time
    constructor:()->
      super
      @tickOffsets = ->
        domain = @graph.x.domain()
        unit = @fixedTimeUnit or @appropriateTimeUnit()
        count = Math.ceil((domain[1] - domain[0]) / unit.seconds)
        stepSize = Math.floor(70 * count /@graph.width)
        if(stepSize <= 0)
          stepSize = 1
        runningTick = domain[0] + @tzOffset
        offsets = []
        i = 0
        while i < count
          tickValue = runningTick
          runningTick = moment(unit.incDate(new Date(runningTick*1000), stepSize)).unix()
          offsets.push
            value: tickValue - @tzOffset
            unit: unit

          i+= stepSize
        offsets

  class Unit
    
    default:
      incDate : (d, i = 1) ->
        Unit.incDate(d, this.name, i)
      diff : (d1, d2) ->
        Unit.diff(d1, d2, this.name)

    year: 
      name: "year"
      seconds: 86400 * 365.25
      hoverFormatter: (d) ->
        d.getFullYear()
      formatter: (d) ->
        d.getFullYear()
      hash: (d) ->
        d.getUTCFullYear()
    
    month:
      name: "month"
      seconds: 86400 * 30.5
      hoverFormatter: (d) ->
        moment(d).format("MMMM YYYY")
      formatter: (d) ->
        moment(d).format("MM/YY")
      hash: (d) ->
        moment(d).utc().format("MM/YYYY")
    
    week:
      name: "week"
      seconds: 86400 * 7
      hoverFormatter: (d) ->
        "week " + moment(d).format("w MMMM YYYY")
      formatter: (d) ->
        moment(d).format("w")
      hash: (d) ->
        moment(d).utc().format("w")
    
    day:
      name: "day"
      seconds: 86400
      hoverFormatter: (d) ->
        moment(d).format('dddd DD MMMM YYYY');
      formatter: (d) ->
        moment(d).format("DD/MM")
      hash: (d) ->
        moment(d).utc().format("DD/MM/YYYY")
    
    hour:
      name: "hour"
      seconds: 3600
      hoverFormatter: (d) ->
        moment(d).format('LLL');
      formatter: (d) ->
        moment(d).format("ha")
      hash: (d) ->
        moment(d).utc().format("DD/MM/YYYY H")
    
    for : (unitName) ->
      _.extend(@default, @[unitName])

    @incDate : (d, interval, i = 1)->
      moment(d).clone().add(interval + 's', i).toDate()

    @diff: (d1, d2, interval)->
      moment(d1).diff(moment(d2), interval + 's')



  CustomHoverDetail: CustomHoverDetail
  XAxis: XAxis
  unit: new Unit()
