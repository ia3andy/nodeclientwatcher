define(function () {
return {

'analytics/content/chart.jade':function (locals){function anonymous(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<h1>OVERALL PAGE VIEWS</h1>');
 if(!chart)
{
buf.push('<div class="col3"><p style="padding:10px;">Chart not available, please, select another interval...</p></div>');
}
 else
{
buf.push('<div class="rickshaw chart-container"><div id="content-pageviews-chart" class="chart"></div><div id="content-pageviews-y_axis" class="y_axis"></div></div>');
}
}
return buf.join("");
}; return anonymous(locals);},
'analytics/content/content.jade':function (locals){function anonymous(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<div class="page-title">Content Overview (Page views)</div><div class="error"></div><div class="content-stats page-block"><h1>OVERALL PAGE VIEWS</h1><div class="loading"><p>Loading...</p></div></div><div class="content-popular-modules page-block col2"><h1>MOST POPULAR PRESENTATIONS</h1><div class="loading"><p>Loading...</p></div></div><div class="content-top-pages page-block col2"><h1>TOP 10 PAGES</h1><div class="loading"><p>Loading...</p></div></div><div class="content-top-games page-block col2"><h1>TOP 10 GAME PLAYS</h1><div class="loading"><p>Loading...</p></div></div><div class="content-top-demo page-block col2"><h1>TOP 10 DEMO MODE PRESENTATIONS</h1><div class="loading"><p>Loading...</p></div></div><div class="clear"></div>');
}
return buf.join("");
}; return anonymous(locals);},
'analytics/content/popular.jade':function (locals){function anonymous(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<h1>' + escape((interp = data.title) == null ? '' : interp) + '</h1>');
 if(data.stats && data.stats.length > 0)
{
buf.push('<table class="stat-list">');
// iterate data.stats
;(function(){
  if ('number' == typeof data.stats.length) {

    for (var key = 0, $$l = data.stats.length; key < $$l; key++) {
      var stat = data.stats[key];

buf.push('<tr class="number"><td><h3>' + escape((interp = stat[data.showAttribute]) == null ? '' : interp) + '</h3></td><td><h4>' + escape((interp = stat.pageViewCounter) == null ? '' : interp) + '</h4></td></tr>');
    }

  } else {
    var $$l = 0;
    for (var key in data.stats) {
      $$l++;      var stat = data.stats[key];

buf.push('<tr class="number"><td><h3>' + escape((interp = stat[data.showAttribute]) == null ? '' : interp) + '</h3></td><td><h4>' + escape((interp = stat.pageViewCounter) == null ? '' : interp) + '</h4></td></tr>');
    }

  }
}).call(this);

buf.push('</table>');
}
 else
{
buf.push('<p>Not available </p>');
}
}
return buf.join("");
}; return anonymous(locals);},
'analytics/content/selection.jade':function (locals){function anonymous(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<h1>MOST POPULAR PRESENTATIONS</h1>');
 if(data.completes && data.completes.length > 0)
{
buf.push('<table class="stat-list"><tr><th colspan="2"><h2>Completions</h2></th></tr>');
// iterate data.completes
;(function(){
  if ('number' == typeof data.completes.length) {

    for (var $index = 0, $$l = data.completes.length; $index < $$l; $index++) {
      var complete = data.completes[$index];

buf.push('<tr class="number slidedeck-complete"> <td><h3>' + escape((interp = complete.slidedeck) == null ? '' : interp) + '</h3></td><td><h4>' + escape((interp = complete.completeCounter) == null ? '' : interp) + '</h4></td></tr>');
    }

  } else {
    var $$l = 0;
    for (var $index in data.completes) {
      $$l++;      var complete = data.completes[$index];

buf.push('<tr class="number slidedeck-complete"> <td><h3>' + escape((interp = complete.slidedeck) == null ? '' : interp) + '</h3></td><td><h4>' + escape((interp = complete.completeCounter) == null ? '' : interp) + '</h4></td></tr>');
    }

  }
}).call(this);

buf.push('</table>');
}
 else
{
buf.push('<p>Not available</p>');
}
}
return buf.join("");
}; return anonymous(locals);},
'analytics/insight/insight.jade':function (locals){function anonymous(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<div class="page-title">InSight</div><div class="error"></div><nav class="slider-control"><a href="#" class="slider-control-prev"><em>prev</em></a><span class="slider-control-position">');
// iterate data.questions
;(function(){
  if ('number' == typeof data.questions.length) {

    for (var $index = 0, $$l = data.questions.length; $index < $$l; $index++) {
      var question = data.questions[$index];

buf.push('<em');
buf.push(attrs({ 'id':("bullet-question-"+question.slide), "class": ('insight-question-bullet') }, {"id":true}));
buf.push('>•</em>');
    }

  } else {
    var $$l = 0;
    for (var $index in data.questions) {
      $$l++;      var question = data.questions[$index];

buf.push('<em');
buf.push(attrs({ 'id':("bullet-question-"+question.slide), "class": ('insight-question-bullet') }, {"id":true}));
buf.push('>•</em>');
    }

  }
}).call(this);

buf.push('</span><a href="#" class="slider-control-next"><em>next</em></a></nav><div class="slider-container"><ul>');
// iterate data.questions
;(function(){
  if ('number' == typeof data.questions.length) {

    for (var $index = 0, $$l = data.questions.length; $index < $$l; $index++) {
      var question = data.questions[$index];

buf.push('<li><div');
buf.push(attrs({ 'id':("question-"+question.slide), "class": ('insight-question') }, {"id":true}));
buf.push('><div class="loading"><p>Loading...</p></div></div></li>');
    }

  } else {
    var $$l = 0;
    for (var $index in data.questions) {
      $$l++;      var question = data.questions[$index];

buf.push('<li><div');
buf.push(attrs({ 'id':("question-"+question.slide), "class": ('insight-question') }, {"id":true}));
buf.push('><div class="loading"><p>Loading...</p></div></div></li>');
    }

  }
}).call(this);

buf.push('</ul></div>');
}
return buf.join("");
}; return anonymous(locals);},
'analytics/insight/question.jade':function (locals){function anonymous(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<div class="question-pie page-block"><h1>' + escape((interp = data.question) == null ? '' : interp) + '</h1><div class="chart-container nv"><div class="insight-question-piechart"><svg></svg></div></div></div><div class="question-total page-block col2"><h1>INSIGHT OVERVIEW</h1><ul class="stat-info">');
 if(data.total && data.total.length == 1)
{
buf.push('<li><h3>Total number of Responses</h3><h4>' + escape((interp = data.total[0].numberCounter) == null ? '' : interp) + '</h4></li><li><h3>Correct Response</h3><h4>' + escape((interp = data.correct) == null ? '' : interp) + '</h4></li><li><h3>Average Response</h3><h4>' + escape((interp = data.total[0].numberAvg) == null ? '' : interp) + '</h4></li><li><h3>Percentage of correct Responses</h3><h4>' + escape((interp = data.correctPercentage) == null ? '' : interp) + '%</h4></li>');
}
buf.push('</ul></div><div class="overview-state page-block col2"><h1>TOP 10 RESPONSES</h1>');
 if(data.top && data.top.length > 0)
{
buf.push('<table class="stat-list">');
// iterate data.top
;(function(){
  if ('number' == typeof data.top.length) {

    for (var $index = 0, $$l = data.top.length; $index < $$l; $index++) {
      var top = data.top[$index];

buf.push('<tr class="number slidedeck-complete"> <td><h3>' + escape((interp = top.numberAvg) == null ? '' : interp) + '</h3></td><td><h4>' + escape((interp = top.numberCounter) == null ? '' : interp) + '</h4></td></tr>');
    }

  } else {
    var $$l = 0;
    for (var $index in data.top) {
      $$l++;      var top = data.top[$index];

buf.push('<tr class="number slidedeck-complete"> <td><h3>' + escape((interp = top.numberAvg) == null ? '' : interp) + '</h3></td><td><h4>' + escape((interp = top.numberCounter) == null ? '' : interp) + '</h4></td></tr>');
    }

  }
}).call(this);

buf.push('</table>');
}
 else
{
buf.push('<p>Not available</p>');
}
buf.push('</div><div class="clear"></div>');
}
return buf.join("");
}; return anonymous(locals);},
'analytics/overview/chart.jade':function (locals){function anonymous(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<h1>OVERALL APP OPENS</h1>');
 if(!chart)
{
buf.push('<div class="col3"><p style="padding:10px;">Chart not available, please, select another interval...</p></div>');
}
 else
{
buf.push('<div class="rickshaw chart-container"><div id="overview-calls-chart" class="chart"></div><div id="overview-calls-y_axis" class="y_axis"></div></div>');
}
}
return buf.join("");
}; return anonymous(locals);},
'analytics/overview/overview.jade':function (locals){function anonymous(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<div class="page-title">Visitors Overview</div><div class="error"></div><div class="overview-stats page-block"><h1>OVERALL APP OPENS</h1><div class="loading"><p>Loading...</p></div></div><div class="overview-total page-block"><h1>OVERALL STATS</h1><div class="loading"><p>Loading...</p></div></div><div class="overview-popular page-block col2"><h1>MOST POPULAR PRESENTATIONS</h1><div class="loading"><p>Loading...</p></div></div><div class="overview-state page-block col2"><h1>STATE TOTALS</h1><div class="loading"><p>Loading...</p></div></div><div class="clear"></div>');
}
return buf.join("");
}; return anonymous(locals);},
'analytics/overview/popular.jade':function (locals){function anonymous(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<h1>MOST POPULAR PRESENTATIONS</h1>');
 if(data.completes && data.completes.length > 0)
{
buf.push('<table class="stat-list"><tr><th colspan="2"><h2>Completions</h2></th></tr>');
// iterate data.completes
;(function(){
  if ('number' == typeof data.completes.length) {

    for (var $index = 0, $$l = data.completes.length; $index < $$l; $index++) {
      var complete = data.completes[$index];

buf.push('<tr class="number slidedeck-complete"> <td><h3>' + escape((interp = complete.slidedeck) == null ? '' : interp) + '</h3></td><td><h4>' + escape((interp = complete.completeCounter) == null ? '' : interp) + '</h4></td></tr>');
    }

  } else {
    var $$l = 0;
    for (var $index in data.completes) {
      $$l++;      var complete = data.completes[$index];

buf.push('<tr class="number slidedeck-complete"> <td><h3>' + escape((interp = complete.slidedeck) == null ? '' : interp) + '</h3></td><td><h4>' + escape((interp = complete.completeCounter) == null ? '' : interp) + '</h4></td></tr>');
    }

  }
}).call(this);

buf.push('</table>');
}
 else
{
buf.push('<p>Not available</p>');
}
}
return buf.join("");
}; return anonymous(locals);},
'analytics/overview/state.jade':function (locals){function anonymous(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<h1>STATE TOTALS</h1>');
 if(data.calls && data.calls.length > 0)
{
buf.push('<table class="stat-list"><tr><th><h2>State</h2></th><th><h2>Calls</h2></th><th><h2>Completions</h2></th>');
// iterate data.calls
;(function(){
  if ('number' == typeof data.calls.length) {

    for (var key = 0, $$l = data.calls.length; key < $$l; key++) {
      var call = data.calls[key];

buf.push('<tr class="number state-complete"> <td><h3>' + escape((interp = call.state) == null ? '' : interp) + '</h3></td><td><h4>' + escape((interp = call.callCounter) == null ? '' : interp) + '</h4></td><td>');
 if(data.completes && data.completes[key])
{
buf.push('<h4>' + escape((interp = data.completes[key].completeCounter) == null ? '' : interp) + '</h4>');
}
buf.push('</td></tr>');
    }

  } else {
    var $$l = 0;
    for (var key in data.calls) {
      $$l++;      var call = data.calls[key];

buf.push('<tr class="number state-complete"> <td><h3>' + escape((interp = call.state) == null ? '' : interp) + '</h3></td><td><h4>' + escape((interp = call.callCounter) == null ? '' : interp) + '</h4></td><td>');
 if(data.completes && data.completes[key])
{
buf.push('<h4>' + escape((interp = data.completes[key].completeCounter) == null ? '' : interp) + '</h4>');
}
buf.push('</td></tr>');
    }

  }
}).call(this);

buf.push('</tr></table>');
}
 else
{
buf.push('<p>Not available</p>');
}
}
return buf.join("");
}; return anonymous(locals);},
'analytics/overview/total.jade':function (locals){function anonymous(locals, attrs, escape, rethrow, merge) {
attrs = attrs || jade.attrs; escape = escape || jade.escape; rethrow = rethrow || jade.rethrow; merge = merge || jade.merge;
var buf = [];
with (locals || {}) {
var interp;
buf.push('<h1>OVERALL STATS</h1><div class="col3"><h2>Total Time</h2><ul class="stat-info">');
 if(data.total && data.total.length == 1)
{
buf.push('<li><h3>App</h3><h4>' + escape((interp = formatDuration(data.total[0].pageViewDuration)) == null ? '' : interp) + '</h4></li>');
}
 if(data.education && data.education.length == 1)
{
buf.push('<li><h3>Education</h3><h4>' + escape((interp = formatDuration(data.education[0].pageViewDuration)) == null ? '' : interp) + '</h4></li>');
}
 if(data.game && data.game.length == 1)
{
buf.push('<li><h3>Game</h3><h4>' + escape((interp = formatDuration(data.game[0].pageViewDuration)) == null ? '' : interp) + '</h4></li>');
}
 if(data.guest && data.guest.length == 1)
{
buf.push('<li><h3>Demo Mode</h3><h4>' + escape((interp = formatDuration(data.guest[0].pageViewDuration)) == null ? '' : interp) + '</h4></li>');
}
buf.push('</ul></div><div class="col3"><h2>Average Time</h2><ul class="stat-info">');
 if(data.call && data.call.length == 1)
{
buf.push('<li><h3>App</h3><h4>' + escape((interp = formatDuration(data.total[0].pageViewDuration / data.call[0].callCounter)) == null ? '' : interp) + '</h4></li>');
 if(data.education && data.education.length == 1)
{
buf.push('<li><h3>Education</h3><h4>' + escape((interp = formatDuration(data.education[0].pageViewDuration / data.call[0].callCounter)) == null ? '' : interp) + '</h4></li>');
}
 if(data.game && data.game.length == 1)
{
buf.push('<li><h3>Game</h3><h4>' + escape((interp = formatDuration(data.game[0].pageViewDuration / data.call[0].callCounter)) == null ? '' : interp) + '</h4></li>');
}
}
buf.push('</ul></div><div class="col3"><h2>Totals</h2><ul class="stat-info">');
 if(data.call && data.call.length == 1)
{
buf.push('<li><h3>Calls</h3><h4>' + escape((interp = data.call[0].callCounter) == null ? '' : interp) + '</h4></li>');
}
 if(data.complete && data.complete.length == 1)
{
buf.push('<li><h3>Presentation completions</h3><h4>' + escape((interp = data.complete[0].completeCounter) == null ? '' : interp) + '</h4></li>');
}
buf.push('</ul></div>');
}
return buf.join("");
}; return anonymous(locals);}
}});