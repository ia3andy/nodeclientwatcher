NodeClientWatcher
=================

Watch less, coffee and jade files/directories and build them

## to release:

* coffee -c -o ./lib/node-client-watcher/ ./src/

## to install:

* clone NodeClientWatcher git project on disk
* install node
* execute: sudo npm install -g git+https://bitbucket.org/ia3andy/nodeclientwatcher.git
* on os x mountain lion, install terminal-notifier: sudo gem install terminal-notifier
* on os x lion, install growl 1.2


## Config file example:

```yaml
less:
  - watch: ['tests/styling/', 'tests/styling2/']
    src: 'tests/styling/all.less'
    target: 'tests/styling/all.css'
  - watch: 'tests/design/'
    src: 'tests/design/all.less'
    target: 'tests/design/all.css'
jade:
  - src: 'tests/templates/'
    target: 'tests/t.js'
coffee:
  - src: 'tests/coffee/'
    target: 'tests/js/'
```

## Run:

```
  Usage: client-watcher [options] <config>

  Options:

    -h, --help     output usage information
    -V, --version  output the version number
    -w, --watch    watch mode
```