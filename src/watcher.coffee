fs = require 'fs'
yaml = require 'js-yaml'
program = require 'commander'
path = require 'path'

coffeeCompiler = require './coffee-compiler'
lessCompiler = require './less-compiler'
jadeCompiler = require './jade-compiler'
module.exports = 
  run: () ->
    program
      .version('0.0.1')
      .usage('[options] <config>')
      .option('-w, --watch', 'watch mode')

    program.parse(process.argv);
    pwd = process.env.PWD

    if !program.args[0]?
      return console.log "Please specify a configuration file."
    configFile = path.resolve(pwd, program.args[0])
    root = path.dirname configFile
    configYml = require configFile 
    config = configYml[0]
    coffeeCompiler.watchCoffee(root, program, config.coffee)
    lessCompiler.watchLess(root, program, config.less)
    jadeCompiler.watchJade(root, program, config.jade)