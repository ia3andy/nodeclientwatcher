fs = require 'fs'
watchr = require 'watchr'
exec = require("child_process").exec
path = require("path")
notify = require("./notify")
exists = fs.existsSync or path.existsSync
os = require("os")
quote = JSON.stringify
cmd = undefined
moment = require 'moment'
utils = {}

red   = '\u001b[31m'
blue  = '\u001b[34m'
reset = '\u001b[0m'
date = () =>
  moment().format('H:mm:ss')

utils.error = (message) ->
  message = "#{red}#{date()} - ERROR: #{reset}\n #{message}"
  console.log message
  logo = path.join __dirname, "../../images/icon.png"
  notify message,
    title: 'ClientWatcher'
    image: logo

utils.log = (message) ->
  message = "#{message}"
  console.log message

utils.logd = (message) ->
  message = "#{date()} - #{message}"
  console.log message


utils.watcher = (towatch, ignore, cb) ->
  config = 
    paths: towatch
    listener: (eventName,filePath,fileCurrentStat,filePreviousStat) ->
      if typeof filePath  == "string" && (!ignore? || !filePath.match(ignore))
        cb(filePath)
    ignoreCommonPatterns: true
    ignoreHiddenFiles: true
  watchr.watch config
    

utils.checkPath = (filepath, type)->
  if(!filepath?)
    return false
  try
    stats = fs.lstatSync filepath
    if(!type)
      if(stats.isDirectory() || stats.isFile())
        return true
    else
      switch type
        when "dir"
          if(stats.isDirectory())
            return true
        when "file"
          if(stats.isFile())
            return true
    
  catch error
    console.log "\t#{filepath} doesn't exist."
  return false

module.exports = utils
