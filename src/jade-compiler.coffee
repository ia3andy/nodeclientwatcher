fs = require 'fs'
{print} = require 'util'
childProcess = require 'child_process'
path = require 'path'
utils = require './utils'
methods = {}

buildJade = (srcFolder, destFolder, minify, callback) ->
  options = [srcFolder, '-O', destFolder]
  if(!minify)
    options.unshift '-P'
  jadec = path.join __dirname, "../../node_modules/jade/bin/jade"
  childProcess.execFile jadec, options, (error, stdout, stderr) ->
    if(stderr? && stderr != "")
      callback(stderr)
    else
      callback()

buildJadeWrapper = (root, items, minify, triggerFile = "") ->
  items.forEach (item) ->
    src = path.join root, item.src
    target = path.join root, item.target
    if utils.checkPath(src) && target?
      name = "#{src} => #{target}"
      buildJade src, target, minify, (err)->
        if(err?)
          utils.error err
        else
          if(triggerFile != "")
            utils.logd "compiled #{name} triggered by #{triggerFile}"
          else
            utils.logd "compiled #{name}"
    else
      utils.error "source or target not valid"


methods.watchJade = (root, program, jadeConfig) ->
  if(jadeConfig?)
    console.log "START JADE:"
    jadeConfig.forEach (jade) ->
      items = jade.items
      minify = jade.minify? && jade.minify
      watch = if!(jade.watch instanceof Array) then [jade.watch] else jade.watch
      if(program.watch)
        paths = []
        watch.forEach (towatch) ->
          towatch = path.normalize(path.join(root, towatch))
          if(utils.checkPath(towatch))
            paths.push towatch
            console.log "watching #{towatch}"
        utils.watcher paths, /.css$/, (filename) ->
          buildJadeWrapper(root, items, minify, filename)
      buildJadeWrapper(root, items, minify)





module.exports = methods