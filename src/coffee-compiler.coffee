fs = require 'fs'
{print} = require 'util'
{spawn} = require 'child_process'
path = require 'path'
utils = require './utils'
methods = {}

### COFFEE ###
methods.watchCoffee = (root, program, coffeeConfig) ->
  if(coffeeConfig?)
    console.log "START COFFEE:"
    for coffee in coffeeConfig
      coffee.src = path.join root, coffee.src
      coffee.target = path.join root, coffee.target
      if utils.checkPath(coffee.src) && utils.checkPath(coffee.target)
        name = "#{coffee.src} => #{coffee.target}"
        if(program.watch) 
          utils.logd "watching: #{name}"
        buildCoffee coffee.src, coffee.target, program.watch


buildCoffee = (srcFolder, destFolder, iswatch, callback) ->
  options = ['-c', '-o', destFolder, srcFolder]
  if(iswatch)
    options.unshift '-w'
  coffeec = path.join __dirname, "../../node_modules/coffee-script/bin/coffee"
  coffee = spawn coffeec, options
  coffee.stderr.on 'data', (data) ->
    utils.error data.toString()
  coffee.stdout.on 'data', (data) ->
    message = data.toString()
    if message.indexOf("In") == 0
      utils.error data.toString()
    else
      utils.log data.toString()
  coffee.on 'exit', (code) ->
    callback?() if code is 0

module.exports = methods