fs = require 'fs'
{print} = require 'util'
childProcess = require 'child_process'
path = require 'path'
utils = require './utils'
methods = {}

### LESS ###


buildLess = (srcFolder, destFolder, minify, callback) ->
  options = [srcFolder, destFolder]
  if(minify)
    options.unshift '-x'
  lessc = path.join __dirname, "../../node_modules/less/bin/lessc"
  childProcess.execFile lessc, options, (error, stdout, stderr) ->
    if(stderr? && stderr != "")
      callback(stderr)
    else
      callback()

buildLessWrapper = (src, target, minify, triggerFile = "") ->
  name = "#{src} => #{target}"
  buildLess src, target, minify, (err)->
    if(err?)
      utils.error err
    else
      if(triggerFile != "")
        utils.logd "compiled #{name} triggered by #{triggerFile}"
      else
        utils.logd "compiled #{name}"

methods.watchLess = (root, program, lessConfig) ->
  if(lessConfig?)
    console.log "START LESS:"
    lessConfig.forEach (less) ->
      
      src = path.join root, less.src
      target = path.join root, less.target
      minify = less.minify? && less.minify
      watch = if!(less.watch instanceof Array) then [less.watch] else less.watch
      if utils.checkPath(src) && target?
        if(program.watch) 
          dirsrc = path.dirname src
          watchsrc = false
          paths = []
          watch.forEach (towatch) ->
           
            towatch = path.normalize(path.join(root, towatch))
            watchsrc =  true if(towatch ==  dirsrc)
            if(utils.checkPath(towatch))
              paths.push towatch
              console.log "watching #{towatch}=> #{target}"  
          utils.watcher paths, /.css$/, (filename) ->
            buildLessWrapper(src, target, minify, filename)
          if(watchsrc)
            console.log "watching #{src}"
            fs.watch src, (event, filename) ->
              buildLessWrapper(filename)
        buildLessWrapper(src, target, minify)





module.exports = methods