which = (name) ->
  paths = process.env.PATH.split(":")
  loc = undefined
  i = 0
  len = paths.length

  while i < len
    loc = path.join(paths[i], name)
    return loc  if exists(loc)
    ++i

clean = (str) ->
  str = str.replace(/[\u001b\[0m\u001b\[34m]/gm, '')
  str = str.replace(/\u0007/, '')
  str = str.replace(/\n/gm, '')

notify = (msg, options, fn) ->
  
  msg = clean msg
  image = undefined
  args = undefined
  options = options or {}
  fn = fn or ->

  # noop
  return fn(new Error("growl not supported on this platform"))  unless cmd
  args = [cmd.pkg]
  
  # image
  if image = options.image
    switch cmd.type
      when "Darwin-Growl"
        flag = undefined
        ext = path.extname(image).substr(1)
        flag = flag or ext is "icns" and "iconpath"
        flag = flag or /^[A-Z]/.test(image) and "appIcon"
        flag = flag or /^png|gif|jpe?g$/.test(ext) and "image"
        flag = flag or ext and (image = ext) and "icon"
        flag = flag or "icon"
        args.push "--" + flag, image
      when "Linux"
        args.push cmd.icon + " " + image
        
        # libnotify defaults to sticky, set a hint for transient notifications
        args.push "--hint=int:transient:1"  unless options.sticky
      when "Windows"
        args.push cmd.icon + quote(image)
  
  # sticky
  args.push cmd.sticky  if options.sticky
  
  # priority
  if options.priority
    priority = options.priority + ""
    checkindexOf = cmd.priority.range.indexOf(priority)
    args.push cmd.priority, options.priority  if ~cmd.priority.range.indexOf(priority)
  
  # name
  args.push "--name", options.name  if options.name and cmd.type is "Darwin-Growl"
  
  switch cmd.type
    when "Darwin-Growl"
      args.push cmd.msg
      args.push quote(msg)
      args.push quote(options.title)  if options.title
    when "Darwin-NotificationCenter"
      args.push cmd.msg
      args.push quote(msg)
      if options.title
        args.push cmd.title
        args.push quote(options.title)
      if options.subtitle
        args.push cmd.subtitle
        args.push quote(options.title)
    when "Darwin-Growl"
      args.push cmd.msg
      args.push quote(msg)
      args.push quote(options.title)  if options.title
    when "Linux"
      if options.title
        args.push quote(options.title)
        args.push cmd.msg
        args.push quote(msg)
      else
        args.push quote(msg)
    when "Windows"
      args.push quote(msg)
      args.push cmd.title + quote(options.title)  if options.title
  
  # execute
  exec args.join(" "), fn
exec = require("child_process").exec
fs = require("fs")
path = require("path")
exists = fs.existsSync or path.existsSync
os = require("os")
quote = JSON.stringify
cmd = undefined
growlnotify = path.join __dirname, "../../bin/growlnotify"
switch os.type()
  when "Darwin"
    if which("terminal-notifier")
      cmd =
        type: "Darwin-NotificationCenter"
        pkg: "terminal-notifier"
        msg: "-message"
        title: "-title"
        subtitle: "-subtitle"
        priority:
          cmd: "-execute"
          range: []
    else
      cmd =
        type: "Darwin-Growl"
        pkg: growlnotify
        msg: "-m"
        sticky: "--sticky"
        priority:
          cmd: "--priority"
          range: [-2, -1, 0, 1, 2, "Very Low", "Moderate", "Normal", "High", "Emergency"]
exports = module.exports = notify