// Generated by CoffeeScript 1.4.0
(function() {
  var buildJade, buildJadeWrapper, childProcess, fs, methods, path, print, utils;

  fs = require('fs');

  print = require('util').print;

  childProcess = require('child_process');

  path = require('path');

  utils = require('./utils');

  methods = {};

  buildJade = function(srcFolder, destFolder, minify, callback) {
    var jadec, options;
    options = [srcFolder, '-O', destFolder];
    if (!minify) {
      options.unshift('-P');
    }
    jadec = path.join(__dirname, "../../node_modules/jade/bin/jade");
    return childProcess.execFile(jadec, options, function(error, stdout, stderr) {
      if ((stderr != null) && stderr !== "") {
        return callback(stderr);
      } else {
        return callback();
      }
    });
  };

  buildJadeWrapper = function(root, items, minify, triggerFile) {
    if (triggerFile == null) {
      triggerFile = "";
    }
    return items.forEach(function(item) {
      var name, src, target;
      src = path.join(root, item.src);
      target = path.join(root, item.target);
      if (utils.checkPath(src) && (target != null)) {
        name = "" + src + " => " + target;
        return buildJade(src, target, minify, function(err) {
          if ((err != null)) {
            return utils.error(err);
          } else {
            if (triggerFile !== "") {
              return utils.logd("compiled " + name + " triggered by " + triggerFile);
            } else {
              return utils.logd("compiled " + name);
            }
          }
        });
      } else {
        return utils.error("source or target not valid");
      }
    });
  };

  methods.watchJade = function(root, program, jadeConfig) {
    if ((jadeConfig != null)) {
      console.log("START JADE:");
      return jadeConfig.forEach(function(jade) {
        var items, minify, paths, watch;
        items = jade.items;
        minify = (jade.minify != null) && jade.minify;
        watch = !(jade.watch instanceof Array) ? [jade.watch] : jade.watch;
        if (program.watch) {
          paths = [];
          watch.forEach(function(towatch) {
            towatch = path.normalize(path.join(root, towatch));
            if (utils.checkPath(towatch)) {
              paths.push(towatch);
              return console.log("watching " + towatch);
            }
          });
          utils.watcher(paths, /.css$/, function(filename) {
            return buildJadeWrapper(root, items, minify, filename);
          });
        }
        return buildJadeWrapper(root, items, minify);
      });
    }
  };

  module.exports = methods;

}).call(this);
