// Generated by CoffeeScript 1.4.0
(function() {
  var buildLess, buildLessWrapper, childProcess, fs, methods, path, print, utils;

  fs = require('fs');

  print = require('util').print;

  childProcess = require('child_process');

  path = require('path');

  utils = require('./utils');

  methods = {};

  /* LESS
  */


  buildLess = function(srcFolder, destFolder, minify, callback) {
    var lessc, options;
    options = [srcFolder, destFolder];
    if (minify) {
      options.unshift('-x');
    }
    lessc = path.join(__dirname, "../../node_modules/less/bin/lessc");
    return childProcess.execFile(lessc, options, function(error, stdout, stderr) {
      if ((stderr != null) && stderr !== "") {
        return callback(stderr);
      } else {
        return callback();
      }
    });
  };

  buildLessWrapper = function(src, target, minify, triggerFile) {
    var name;
    if (triggerFile == null) {
      triggerFile = "";
    }
    name = "" + src + " => " + target;
    return buildLess(src, target, minify, function(err) {
      if ((err != null)) {
        return utils.error(err);
      } else {
        if (triggerFile !== "") {
          return utils.logd("compiled " + name + " triggered by " + triggerFile);
        } else {
          return utils.logd("compiled " + name);
        }
      }
    });
  };

  methods.watchLess = function(root, program, lessConfig) {
    if ((lessConfig != null)) {
      console.log("START LESS:");
      return lessConfig.forEach(function(less) {
        var dirsrc, minify, paths, src, target, watch, watchsrc;
        src = path.join(root, less.src);
        target = path.join(root, less.target);
        minify = (less.minify != null) && less.minify;
        watch = !(less.watch instanceof Array) ? [less.watch] : less.watch;
        if (utils.checkPath(src) && (target != null)) {
          if (program.watch) {
            dirsrc = path.dirname(src);
            watchsrc = false;
            paths = [];
            watch.forEach(function(towatch) {
              towatch = path.normalize(path.join(root, towatch));
              if (towatch === dirsrc) {
                watchsrc = true;
              }
              if (utils.checkPath(towatch)) {
                paths.push(towatch);
                return console.log("watching " + towatch + "=> " + target);
              }
            });
            utils.watcher(paths, /.css$/, function(filename) {
              return buildLessWrapper(src, target, minify, filename);
            });
            if (watchsrc) {
              console.log("watching " + src);
              fs.watch(src, function(event, filename) {
                return buildLessWrapper(filename);
              });
            }
          }
          return buildLessWrapper(src, target, minify);
        }
      });
    }
  };

  module.exports = methods;

}).call(this);
